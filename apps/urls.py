from django.urls import path
from apps import views
from . import views

app_name = 'apps'

urlpatterns = [
    path('', views.home),
    path('home/', views.home),
    path('indexayam/', views.indexayam),
    path('indexikan/', views.indexikan),
    path('indexkambing/', views.indexkambing),
    path('indexsapi/', views.indexsapi),
    path('result/', views.result),
    path('resultayam/', views.resultayam),
    path('resultikan/', views.resultikan),
    path('resultkambing/', views.resultkambing),
    path('resultsapi/', views.resultsapi),
    path('resep/<int:id>', views.resep, name='resep'),
    path('resepayam/<int:id>', views.resepayam, name='resepayam'),
    path('resepikan/<int:id>', views.resepikan, name='resepikan'),
    path('resepkambing/<int:id>', views.resepkambing, name='resepkambing'),
    path('resepsapi/<int:id>', views.resepsapi, name='resepsapi'),

]