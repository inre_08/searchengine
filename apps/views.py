from django.shortcuts import render
from django.http import HttpResponse
from apps.inverted import main, mainayam, mainikan, mainkambing, mainsapi

# Create your views here.
# def index(request):
#     return HttpResponse("Hello, world. You're at the polls index.")

def home(request):
    return render(request, 'apps/home.html')

def indexayam(request):
    return render(request, 'apps/indexayam.html')

def indexikan(request):
    return render(request, 'apps/indexikan.html')

def indexkambing(request):
    return render(request, 'apps/indexkambing.html')

def indexsapi(request):
    return render(request, 'apps/indexsapi.html')

def indextahu(request):
    return render(request, 'apps/indextahu.html')

def indextelur(request):
    return render(request, 'apps/indextelur.html')

def indextempe(request):
    return render(request, 'apps/indextempe.html')

def indexudang(request):
    return render(request, 'apps/indexudang.html')



def result(request):
    if request.method == 'POST':
        query = request.POST['querysearch']
        hasil = main.main(query)

        content={
            'hasil':hasil,
            'query':query
        }
    return render(request, 'apps/result.html', content)

def resultayam(request):
    if request.method == 'POST':
        query = request.POST['querysearch']
        hasil = mainayam.main(query)

        content={
            'hasil':hasil,
            'query':query
        }
    return render(request, 'apps/resultayam.html', content)

def resultikan(request):
    if request.method == 'POST':
        query = request.POST['querysearch']
        hasil = mainikan.main(query)

        content={
            'hasil':hasil,
            'query':query
        }
    return render(request, 'apps/resultikan.html', content)

def resultkambing(request):
    if request.method == 'POST':
        query = request.POST['querysearch']
        hasil = mainkambing.main(query)

        content={
            'hasil':hasil,
            'query':query
        }
    return render(request, 'apps/resultkambing.html', content)

def resultsapi(request):
    if request.method == 'POST':
        query = request.POST['querysearch']
        hasil = mainsapi.main(query)

        content={
            'hasil':hasil,
            'query':query
        }
    return render(request, 'apps/resultsapi.html', content)




def resep(request,id):
    text, judul, step = main.detail(id)
    content={
        'no': id,
        'Title':judul,
        'Ingredients':text,
        'Steps':step
    }
    return render(request, 'apps/resep.html', content)

def resepayam(request,id):
    text, judul, step = mainayam.detail(id)
    content={
        'no': id,
        'Title':judul,
        'Ingredients':text,
        'Steps':step
    }
    return render(request, 'apps/resepayam.html', content)

def resepikan(request,id):
    text, judul, step = mainikan.detail(id)
    content={
        'no': id,
        'Title':judul,
        'Ingredients':text,
        'Steps':step
    }
    return render(request, 'apps/resepikan.html', content)

def resepkambing(request,id):
    text, judul, step = mainkambing.detail(id)
    content={
        'no': id,
        'Title':judul,
        'Ingredients':text,
        'Steps':step
    }
    return render(request, 'apps/resepkambing.html', content)

def resepsapi(request,id):
    text, judul, step = mainikan.detail(id)
    content={
        'no': id,
        'Title':judul,
        'Ingredients':text,
        'Steps':step
    }
    return render(request, 'apps/resepsapi.html', content)