"""searchengine URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from apps import views

urlpatterns = [
    path('', views.home),
    path('home', views.home),
    path('indexayam/', views.indexayam),
    path('indexikan/', views.indexikan),
    path('indexkambing/', views.indexkambing),
    path('indexsapi/', views.indexsapi),
    path('result/', views.result),
    path('resultayam/', views.resultayam),
    path('resultikan/', views.resultikan),
    path('resultkambing/', views.resultkambing),
    path('resultsapi/', views.resultsapi),
    path('resep/<int:id>', views.resep, name='resep'),
    path('resepayam/<int:id>', views.resepayam, name='resepayam'),
    path('resepikan/<int:id>', views.resepikan, name='resepikan'),
    path('resepkambing/<int:id>', views.resepkambing, name='resepkambing'),
    path('resepsapi/<int:id>', views.resepsapi, name='resepsapi'),
    path('apps/', include('apps.urls')),
]
